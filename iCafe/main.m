//
//  main.m
//  iCafe
//
//  Created by Dmitry Sazanovich on 16/03/2014.
//  Copyright (c) 2014 Dmitry Sazanovich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BWSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BWSAppDelegate class]));
    }
}
