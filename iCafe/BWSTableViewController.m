//
//  BWSTableViewController.m
//  iCafe
//
//  Created by Dmitry Sazanovich on 16/03/2014.
//  Copyright (c) 2014 Dmitry Sazanovich. All rights reserved.
//

#import "BWSTableViewController.h"

@interface BWSTableViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation BWSTableViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:UITableViewCell.class forCellReuseIdentifier:NSStringFromClass([self class])];
    [self.view addSubview:_tableView];
}
- (void)viewDidLayoutSubviews{
    _tableView.frame = self.view.bounds;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([self class])];
    cell.textLabel.text = @"text";
    
    return cell;
}

@end
