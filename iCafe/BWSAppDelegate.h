//
//  BWSAppDelegate.h
//  iCafe
//
//  Created by Dmitry Sazanovich on 16/03/2014.
//  Copyright (c) 2014 Dmitry Sazanovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BWSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController* tabBarController;

@end
