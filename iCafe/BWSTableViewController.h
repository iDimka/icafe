//
//  BWSTableViewController.h
//  iCafe
//
//  Created by Dmitry Sazanovich on 16/03/2014.
//  Copyright (c) 2014 Dmitry Sazanovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BWSTableViewController : UIViewController

@property (nonatomic, strong) UITableView* tableView;

@end
