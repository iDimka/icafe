//
//  BWSFirstViewController.m
//  iCafe
//
//  Created by Dmitry Sazanovich on 16/03/2014.
//  Copyright (c) 2014 Dmitry Sazanovich. All rights reserved.
//

#import "BWSFirstViewController.h"

@interface BWSFirstViewController () 

@end

@implementation BWSFirstViewController


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([self class])];
    cell.textLabel.text = @"text";
    
    return cell;
}

@end
